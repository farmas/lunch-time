import { FlowRouter } from 'meteor/kadira:flow-router';
import { ReactiveVar } from 'meteor/reactive-var';
import postal from 'postal';

import './recipeViewDialog.html';
import '../parts/recipeDetailsPart.js';

Template.recipeViewDialog.onCreated(function(){
    var template = this;
    template.activeRecipeTemplate = new ReactiveVar();
    template.activeRecipeTemplateData = new ReactiveVar({});

    template.viewRecipeSub = postal.subscribe({
        topic: 'recipe.view',
        callback: function(data) {
            template.activeRecipeTemplateData.set({ recipeId: data.id });
            template.activeRecipeTemplate.set('recipeDetailsPart');
            $('#recipeViewDialog').modal('show');
        }
    });
});

Template.recipeViewDialog.onRendered(function() {
    var template = this;
    $('#recipeViewDialog').on('hide.bs.modal', function() {
        template.activeRecipeTemplate.set('');
        template.activeRecipeTemplateData.set({});
    });
});

Template.recipeViewDialog.onDestroyed(function() {
    postal.unsubscribe(this.viewRecipeSub);
});

Template.recipeViewDialog.helpers({
    activeRecipeTemplate() {
        return Template.instance().activeRecipeTemplate.get();
    },
    activeRecipeTemplateData() {
        return Template.instance().activeRecipeTemplateData.get();
    },
    recipeUrl() {
        var recipeId = Template.instance().activeRecipeTemplateData.get().recipeId;
        var path = FlowRouter.path('recipes.view', { recipeId: recipeId });
        return window.location.origin + path;
    }
});