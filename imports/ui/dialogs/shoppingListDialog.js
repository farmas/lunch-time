import { FlowRouter } from 'meteor/kadira:flow-router';
import { ReactiveVar } from 'meteor/reactive-var';
import * as OrderMethods from '../../api/orders/orders.methods.js';
import postal from 'postal';

import './shoppingListDialog.html';
import '../parts/shoppingListPart';

Template.shoppingListDialog.onCreated(function() {
    var template = this;

    this.shoppingListTemplate = new ReactiveVar();
    this.shoppingListTemplateData = new ReactiveVar();

    template.viewShoppingList = postal.subscribe({
        topic: 'shoppingList.show',
        callback: function(data) {
            console.log('shoppingList.show', data);

             if (data.meals) {
                Session.set('meals', data.meals);
            }

            template.shoppingListTemplate.set('shoppingListPart');
            template.shoppingListTemplateData.set({ orderId: data.orderId });

            $('#shoppingListDialog').modal('show');
        }
    });
});

Template.shoppingListDialog.onRendered(function() {
    var template = this;
     $('#shoppingListDialog').on('hide.bs.modal', function() {
        template.shoppingListTemplate.set('');
    });
})

Template.shoppingListDialog.onDestroyed(function() {
    postal.unsubscribe(this.viewShoppingList);
});

Template.shoppingListDialog.helpers({
    shoppingListTemplate() {
        return Template.instance().shoppingListTemplate.get();
    },
    shoppingListTemplateData() {
        return Template.instance().shoppingListTemplateData.get();
    },
    shoppingListUrl() {
        var data = Template.instance().shoppingListTemplateData.get();
        var path = FlowRouter.path('orders.shoppingList', { orderId: data && data.orderId });
        return window.location.origin + path;
    }
});

Template.shoppingListDialog.events({
    'click .shopping-list-email': function(event, template) {
        event.preventDefault();

        var $email = $('#shoppingListDialog .email-to');
        var data = template.shoppingListTemplateData.get();

        OrderMethods.mailShoppingList.call({ orderId: data && data.orderId, email: $email.val() }, function(err,res) {
            if (!handleError(err)) {
                $email.val('');
                postal.publish({
                    topic: 'toast',
                    data: {
                        message: 'Shopping list has been sent to your email.'
                    }
                });
            }
        });
    }
});

function handleError(err) {
    if (err) {
        postal.publish({
            topic: 'toast',
            data: {
                error: err
            }
        });
        return true;
    }
}