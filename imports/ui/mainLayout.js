import { FlowRouter } from 'meteor/kadira:flow-router';
import { ReactiveVar } from 'meteor/reactive-var';
import { ActiveRoute} from 'meteor/zimme:active-route';
import postal from 'postal';
import toastr from 'toastr';

import './mainLayout.html';
import './dialogs/recipeViewDialog.js';
import './dialogs/shoppingListDialog.js';

Template.mainLayout.onRendered(function() {
    this.toastSub = postal.subscribe({
        topic: 'toast',
        callback: (data) => {
            if (data.error) {
                console.log('Error: ', data.error);
                var errorMessage = data.message || 'There was an error performing an operation. Reason: ' + data.error.reason;
                toastr.error(errorMessage);
            } else {
                var type = data.type || 'success';
                toastr[type](data.message);
            }
        }
    });

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-full-width",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
});

Template.mainLayout.onDestroyed(function(){
    postal.unsubscribe(this.toastSub);
});

Template.mainLayout.helpers({
    navItems() {
        return _.filter(FlowRouter._routes, r => r.group && r.group.name === 'navigatable');
    },
    error() {
        return Template.instance().error.get();
    },
    isSectionActive(navRoute) {
        if (ActiveRoute.name(navRoute.name)) {
            return 'active';
        }

        var currentRouteName = FlowRouter.getRouteName();
        var currentRoute = FlowRouter._routes.find(r => r.name === currentRouteName);

        return currentRoute.options.section === navRoute.name ? 'active' : '';
    }
});