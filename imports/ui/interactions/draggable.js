import interact from 'interact.js';

export const setupDraggable = function(selector, enabled, createShadowFunc) {
    var interaction = interact(selector).draggable({
        inertia: true,
        enabled: !!enabled,
        autoScroll: true,
        onmove: dragMoveListener,
        onend: function (event) {
          // removed the cloned draggable element
          var target = event.target;
          target.parentNode.removeChild(target);
        }
      });

    interaction.on('move', function (event) {
        var clone;
        var interaction = event.interaction;
        var original = event.currentTarget;

      // if the pointer was moved while being held down
      // and an interaction hasn't started yet
      if (interaction.pointerIsDown && !interaction.interacting()) {

        // create a clone of the currentTarget element
        if (createShadowFunc) {
            clone = createShadowFunc(original);
        } else {
            clone = original.cloneNode(true);

            clone.style.width = original.offsetWidth + 'px';
            clone.style.height = original.offsetHeight + 'px';
            clone.style.position = 'absolute';
            clone.style.top = original.offsetTop + 'px';
            clone.style.left = original.offsetLeft + 'px';
            clone.style.zIndex = 3;

            // insert the clone to the page and position it
            original.parentNode.appendChild(clone);
        }

        // start a drag interaction targeting the clone
        interaction.start({ name: 'drag' }, event.interactable, clone);

      } else {
        interaction.start({ name: 'drag' }, event.interactable, original);
      }
    })
}

function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }