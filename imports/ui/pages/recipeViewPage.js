import { FlowRouter } from 'meteor/kadira:flow-router';
import './recipeViewPage.html';
import '../parts/recipeDetailsPart.js';

Template.recipeViewPage.helpers({
    recipeId() {
        return FlowRouter.getParam('recipeId');
    }
});