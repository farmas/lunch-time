import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/kadira:flow-router';
import './orderPage.html';
import '../parts/ingredientsListPart.js';
import '../parts/recipesListPart.js';
import '../parts/orderDetailsPart.js';

var tabs = [
            { name: 'Recipes', template: 'recipesListPart' },
            { name: 'Ingredients', template: 'ingredientsListPart' }
           ];

Template.orderPage.onCreated(function(){
    this.activeTabTemplate = new ReactiveVar(tabs[0].template);
});

Template.orderPage.helpers({
    tabs() {
        return tabs;
    },
    activeTabTemplate() {
        return Template.instance().activeTabTemplate.get();
    },
    isActiveTabTemplate(tabTemplate) {
        return Template.instance().activeTabTemplate.get() === tabTemplate;
    },
    tabTemplateData() {
        return {
            enableSelection: false,
            enableViewDetails: true,
            enableDraggable: true
        }
    }
});

Template.orderPage.events({
    'click .component-tab': function(event, template) {
        event.preventDefault();

        var $tab = $(event.currentTarget);
        var tabTemplate = $tab.data('template');

        template.activeTabTemplate.set(tabTemplate);
    }
});
