import { Meteor } from 'meteor/meteor';
import { FindFromPublication} from 'meteor/percolate:find-from-publication';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Orders } from '../../api/orders/orders.js';
import { Meals } from '../../api/meals/meals.js';
import { MeasurementTypes } from '../../api/recipes/recipes.js';
import { formatMeasurement } from '../../api/orders/orders.methods.js';

import './orderViewMobilePage.html';

var LocalMeals = new Mongo.Collection(null);

Template.orderViewMobilePage.onCreated(function() {
    this.subscribe('orderAndMeals', FlowRouter.getParam('orderId'));

    if (LocalMeals.find().count() < 1) {
        LocalMeals.insert({
            name: 'Please sign in to access your meals.',
            items: []
        });
    }
});

Template.orderViewMobilePage.helpers({
    meals() {
        return _.sortBy(getMealsCursor().fetch(), function(meal) { return meal.order || 1 });
    },
    measurementTypes() {
        return MeasurementTypes;
    },
    isRecipe(item) {
        return item && item.type === 'recipe';
    },
    foodAmount(food) {
        if (food.type === 'ingredient') {
            var measurement = formatMeasurement(food);

            if (measurement) {
                return `(${measurement})`;
            }
        }
    },
    recipeUrl(food) {
        if (food && food.type === 'recipe') {
            var path = FlowRouter.path('recipes.view', { recipeId: food.id });
            return window.location.origin + path;
        }
    }
});

function getMealsCursor() {
    var orderId = FlowRouter.getParam('orderId');
    var publication = orderId ? 'orderAndMeals' : 'lastOrderAndMeals';

    if (orderId || Meteor.userId()) {
        var order = Orders.findOne();
        var mealsMap = order && order.meals && _.reduce(order.meals, function(map, val, index) {
            map[val] = index + 1;
            return map;
        }, {});

        return Meals.findFromPublication(publication, {}, {
            sort: { createdDate: 1 },
            transform: function(meal) {
                var mealOrder = mealsMap && mealsMap[meal._id];
                meal.order = mealOrder || 100;
                return meal;
            }
        });
    } else {
        return LocalMeals.find();
    }
}