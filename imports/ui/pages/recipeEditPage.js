import { FlowRouter } from 'meteor/kadira:flow-router';
import './recipeEditPage.html';
import '../parts/ingredientsListPart.js';
import '../parts/recipeEditPart.js';

Template.recipeEditPage.helpers({
    recipeId() {
        return FlowRouter.getParam('recipeId');
    }
});