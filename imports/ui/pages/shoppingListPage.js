import { FlowRouter } from 'meteor/kadira:flow-router';
import './shoppingListPage.html';
import '../parts/shoppingListPart.js';

Template.shoppingListPage.helpers({
    orderId() {
        return FlowRouter.getParam('orderId');
    }
});

Template.shoppingListPage.events({
    'click .order-print': function(event) {
        event.preventDefault();

        window.print();
    }
})