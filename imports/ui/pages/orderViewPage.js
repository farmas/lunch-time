import './orderViewPage.html';

Template.orderViewPage.helpers({
    orderId() {
        return FlowRouter.getParam('orderId');
    }
});