import { Ingredients } from '../../api/ingredients/ingredients.js';
import { IngredientSchema } from '../../api/ingredients/ingredients.js';
import { Categories } from '../../api/ingredients/ingredients.js';
import * as IngredientMethods from '../../api/ingredients/ingredients.methods.js';
import { ReactiveVar } from 'meteor/reactive-var';
import { FindFromPublication} from 'meteor/percolate:find-from-publication';
import { Random } from 'meteor/random';
import { _ } from 'meteor/underscore';
import { setupDraggable } from '../interactions/draggable.js';
import postal from 'postal';

import './ingredientsListPart.html';

Template.ingredientsListPart.onCreated(function ingredientsListPartOnCreated() {
    var instance = this;

    instance.limit = new ReactiveVar(5);
    instance.query = new ReactiveVar();
    instance.onlyMine = new ReactiveVar(false);
    instance.addIngredientError = new ReactiveVar();

    instance.autorun(function() {
        var limit = instance.limit.get();
        var query = instance.query.get();
        var onlyMine = instance.onlyMine.get();

        instance.subscribe('ingredients', limit, onlyMine, query);
    });

    instance.dataCursor = function() {
        return Ingredients.findFromPublication('ingredients', {}, { sort: { name: 1 }});
    }
});

Template.ingredientsListPart.onRendered(function() {
    var template = this;

    if (this.data.autoFocus) {
        var searchTextbox = this.findAll('.ingredient-search')[0];
        $(searchTextbox).focus();
    }

    setupDraggable('.ingredientsListPart .ingredient-item', this.data.enableDraggable);

    $('#addIngredientModal').on('show.bs.modal', function() {
        template.addIngredientError.set();
    });

    $('#addIngredientModal').on('shown.bs.modal', function() {
        $('#inputIngredientName').focus();
    });

    $('#addIngredientModal').on('hide.bs.modal', function() {
        $('#inputIngredientName').val('');
        $('#inputIngredientImageUrl').val('');
        $('#inputIngredientId').val('');
        $('#inputIngredientCategory').val('');
    });
});

Template.ingredientsListPart.helpers({
    ingredients() {
        return Template.instance().dataCursor();
    },
    hasAny() {
        return Template.instance().dataCursor().count() > 0;
    },
    hasMore() {
        return Template.instance().dataCursor().count() >= Template.instance().limit.get();
    },
    addIngredientError() {
        return Template.instance().addIngredientError.get();
    },
    categories() {
        return [''].concat(Categories);
    }
});

Template.ingredientsListPart.events({
    'click .ingredient-edit': function(event, template) {
        event.preventDefault();

        var ingredientId = this['_id'];

        var ingredient = Ingredients.findOne(ingredientId);

        if (ingredient) {
            $('#inputIngredientId').val(ingredient._id);
            $('#inputIngredientName').val(ingredient.name);
            $('#inputIngredientImageUrl').val(ingredient.imageUrl);
            $('#inputIngredientCategory').val(ingredient.category);

            $('#addIngredientModal').modal('show');
        }
    },
    'click .ingredient-show': function(event, template) {
        event.preventDefault();
        $('#inputIngredientId').val(Random.id());
        $('#addIngredientModal').modal('show');
    },
    'click .ingredient-delete': function(event, template) {
        event.preventDefault();

        if(confirm('Are you sure you want to delete this ingredient?')) {
            var ingredientId = this['_id'];
            IngredientMethods.removeIngredient.call({ id: ingredientId }, methodReturnHandler);
        }
    },
    'click .ingredient-save': function(event, template) {
        event.preventDefault();
        template.addIngredientError.set();

        var name = $('#inputIngredientName').val();
        var validationContext = IngredientSchema.namedContext();
        var newIngredient = IngredientSchema.clean({
            _id: $('#inputIngredientId').val(),
            name: name,
            category: $('#inputIngredientCategory').val(),
            imageUrl: $('#inputIngredientImageUrl').val()
        });

        if(!validationContext.validate(newIngredient)) {
            // invalid ingredient, stay on modal and show error
            var errorObj = validationContext.getErrorObject();
            console.log(errorObj);
            template.addIngredientError.set(errorObj.message);
        } else {
            // dismiss modal and call server.
            $('#addIngredientModal').modal('hide');
            $('.ingredient-search').val(name);
            template.query.set(name);
            template.limit.set(5);

            IngredientMethods.addOrUpdate.call({ ingredient: newIngredient }, methodReturnHandler);
        }
    },
    'click .ingredients-load-more': function(event, template) {
        event.preventDefault();
        var newLimit = template.limit.get() + 5;

        template.limit.set(newLimit);
    },
    'click .ingredient-search-mine': function(event, template) {
        template.onlyMine.set(event.currentTarget.checked);
        template.limit.set(5);
    },
    'keyup .ingredient-search': _.debounce(function(event, template) {
        event.preventDefault();

        template.query.set(event.target.value);
        template.limit.set(5);

    }, 300)
});

function methodReturnHandler(err, res) {
    if (err) {
        postal.publish({
            topic: 'toast',
            data: {
                error: err
            }
        });
    }
}