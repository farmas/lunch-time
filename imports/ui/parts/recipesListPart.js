import './recipesListPart.html';
import { ReactiveVar } from 'meteor/reactive-var';
import { Recipes } from '../../api/recipes/recipes.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import * as RecipeMethods from '../../api/recipes/recipes.methods.js';
import { Random } from 'meteor/random';
import { setupDraggable } from '../interactions/draggable.js';
import postal from 'postal';

Template.recipesListPart.onCreated(function recipesListPartOnCreated() {
    var template = this;

    template.limit = new ReactiveVar(5);
    template.query = new ReactiveVar();
    template.selectedItem = new ReactiveVar();

    template.autorun(() => {
        template.subscribe('recipes', template.limit.get(), template.query.get());
    });

    template.dataCursor = function() {
        return Recipes.findFromPublication('recipes', {}, { sort: { name: 1 }});
    }
});

Template.recipesListPart.onRendered(function() {
    if (this.data.autoFocus) {
        var searchTextbox = this.findAll('.recipe-search')[0];
        $(searchTextbox).focus();
    }

    setupDraggable('.recipesListPart .recipe-item', this.data.enableDraggable);
});

Template.recipesListPart.helpers({
    recipes() {
        return Template.instance().dataCursor();
    },
    hasAny() {
        return Template.instance().dataCursor().count() > 0;
    },
    hasMore() {
        return Template.instance().dataCursor().count() >= Template.instance().limit.get();
    },
    isSelected(item) {
        return item['_id'] === Template.instance().selectedItem.get();
    },
    canViewDetails() {
        return !!Template.instance().data.enableViewDetails;
    },
    recipeEditUrl(recipe) {
        var path = FlowRouter.path('recipes.edit', { recipeId: recipe._id });
        return window.location.origin + path;
    }
});

Template.recipesListPart.events({
    'click .recipe-view': function(event, template) {
        event.preventDefault();
        event.stopPropagation();

        var recipeId = $(event.currentTarget).parents('.recipe-item').data('id');

        postal.publish({
            topic: 'recipe.view',
            data: {
                id: recipeId
            }
        });
    },
    'click .recipe-add': function(event, template) {
        event.preventDefault();

        FlowRouter.go('recipes.edit', { recipeId: Random.id() }, { origin: FlowRouter.getRouteName() });
    },
    'click .recipe-delete': function(event, template) {
        event.preventDefault();
        event.stopPropagation();

        if (confirm('Are you sure you want to delete this recipe?')) {
            var recipeId = $(event.currentTarget).parents('.recipe-item').data('id');
            RecipeMethods.removeRecipe.call({ id: recipeId }, function(err) {
                if (err) {
                    postal.publish({ topic: 'toast', data: { error: err }});
                }
            });
        }
    },
    'click .recipes-load-more': function(event, template) {
        event.preventDefault();

        var newLimit = template.limit.get() + 5;
        template.limit.set(newLimit);
    },
    'keyup .recipe-search': _.debounce(function(event, template) {
        event.preventDefault();

        template.query.set(event.target.value);
        template.limit.set(5);
    }, 300),
    'click .recipe-item': function(event, template) {
        if (template.data.enableSelection) {
            var recipeId = $(event.currentTarget).data('id');
            template.selectedItem.set(recipeId);

            postal.publish({
                topic: 'recipe.selected',
                data: {
                    id: recipeId
                }
            });
        }
    }
});