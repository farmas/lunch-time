import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Random } from 'meteor/random';
import { Session } from 'meteor/session'
import { ReactiveVar } from 'meteor/reactive-var';
import { FindFromPublication} from 'meteor/percolate:find-from-publication';
import { Meals } from '../../api/meals/meals.js';
import { Orders } from '../../api/orders/orders.js';
import { Ingredients } from '../../api/ingredients/ingredients.js';
import { Recipes } from '../../api/recipes/recipes.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { MeasurementTypes } from '../../api/recipes/recipes.js';
import { setupDraggable } from '../interactions/draggable.js';
import * as OrderMethods from '../../api/orders/orders.methods.js';
import * as MealMethods from '../../api/meals/meals.methods.js';
import postal from 'postal';
import interact from 'interact.js';
import dragula from 'dragula';
import toastr from 'toastr';

import './orderDetailsPart.html';
import './foodItemPart.js';
import './shoppingListPart.js';

var LocalMeals = new Mongo.Collection(null);

Template.orderDetailsPart.onCreated(function() {
    var template = this;

    if (this.data.orderId) {
        this.subscribe('orderAndMeals', this.data.orderId);
    } else {
        this.subscribe('lastOrderAndMeals');

        this.autorun(function() {
            if (Meteor.userId()) {
                OrderMethods.ensureOrderExists.call({}, function (err, res) {
                    if (!handleError(err)) {
                        template.subscribe('lastOrderAndMeals');
                    }
                });
            }
        });
    }

    if (LocalMeals.find().count() < 1) {
        LocalMeals.insert({
            name: 'Sample Meal (Local)',
            items: []
        });
    }
});

Template.orderDetailsPart.onRendered(function() {
    setupPostalSubscriptions();

    setupDropTargetOfMealItems();
    setupDropTargetOfMeals();
    setupDraggableOfMealItems();
});

Template.orderDetailsPart.onDestroyed(function(){
    postal.unsubscribe(this.foodRemovedSub);
    postal.unsubscribe(this.ingredientEditSub);
});

Template.orderDetailsPart.helpers({
    meals() {
        return _.sortBy(getMealsCursor().fetch(), function(meal) { return meal.order || 1 });
    },
    canDeleteMeal() {
        return Template.instance().data.allowUpdates && getMealsCursor().count() > 1 && isAuthorizedToUpdateMeal();
    },
    isAuthorizedToUpdateMeal() {
        return Template.instance().data.allowUpdates && isAuthorizedToUpdateMeal();
    },
    shoppingListUrl() {
        var path = FlowRouter.path('orders.shoppingList', { orderId: getOrderId() });
        return window.location.origin + path;
    },
    orderViewUrl() {
        var path = FlowRouter.path('orders.view', { orderId: getOrderId() });
        return window.location.origin + path;
    },
    orderMobileViewUrl() {
        var path = FlowRouter.path('orders.mobile.view', { orderId: getOrderId() });
        return window.location.origin + path;
    },
    measurementTypes() {
        return MeasurementTypes;
    }
});

Template.orderDetailsPart.events({
    'click .ingredient-save': function(event, template) {
        event.preventDefault();

        var $modal = $('#mealIngredientModal');
        var mealId = $modal.find('.meal-id').val();
        var ingredientId = $modal.find('.ingredient-id').val();
        var measurement = $modal.find('.ingredient-quantity').val();
        var measurementType = $modal.find('.ingredient-type').val();

        editIngredient(mealId, ingredientId, measurement, measurementType);

        $modal.modal('hide');
    },
    'click .order-process': function(event, template) {
        event.preventDefault();

        postal.publish({
            topic: 'shoppingList.show',
            data: {
                orderId: getOrderId(),
                meals: !Meteor.userId() && getMealsCursor().fetch()
            }
        });
    },
    'blur .meal-name': function(event, template) {
        event.preventDefault();

        var mealId = $(event.target).parents('.meal').data('id');
        var mealName = event.target.value;

        if (Meteor.userId()) {
            MealMethods.renameMeal.call({ mealId: mealId, name: mealName }, methodReturnHandler);
        } else {
            var meal = getMeal(mealId);
            meal.name = event.target.value;
            LocalMeals.update(meal['_id'], meal);
        }
    },
    'click .add-meal': function(event, template) {
        event.preventDefault();

        if (Meteor.userId()) {
            MealMethods.addMeal.call({
                name: 'Sample Meal (server)',
                orderId: getOrderId()
            }, methodReturnHandler);
        } else {
            LocalMeals.insert({
                name: 'Sample Meal (local)',
                items: []
            });
        }
    },
    'click .remove-meal': function(event, template) {
        event.preventDefault();

        var mealId = $(event.target).parents('.meal').data('id');
        var orderId = getOrderId();
        if (Meteor.userId()) {
            MealMethods.removeMeal.call({ mealId: mealId, orderId: orderId }, methodReturnHandler);
        } else {
            LocalMeals.remove(mealId);
        }
    },
    'click .add-placeholder': function(event, template) {
        var mealId = $(event.target).parents('.meal').data('id');
        var placeholderName = window.prompt('Add an item (not included in shopping list):');

        if (placeholderName) {
            addPlaceholderFood(mealId, placeholderName);
        }
    }
});

function setupPostalSubscriptions() {
    this.foodRemovedSub = postal.subscribe({
        topic: 'food.removed',
        callback: (data) => {
            removeFood(data.mealId, data.food);
        }
    });

    this.ingredientEditSub = postal.subscribe({
        topic: 'ingredient.edit',
        callback: (data) => {
            var ingredient =data.ingredient;
            var $modal = $('#mealIngredientModal');

            $modal.find('.meal-id').val(data.mealId);
            $modal.find('.ingredient-id').val(ingredient._id);
            $modal.find('.ingredient-name').text(ingredient.name);
            $modal.find('.ingredient-quantity').val(ingredient.measurement);
            $modal.find('.ingredient-type').val(ingredient.measurementType);
            $modal.modal('show');
        }
    });
}

function setupDropTargetOfMeals() {
    if (!Meteor.userId()) {
        return;
    }

    var mealHtmlList = $('.meal-list')[0];

    var drake = dragula([mealHtmlList], {
        moves: function(el, container, handle) {
            var isHandle = $(handle).hasClass('fa-bars');

            return isHandle;
        }
    }).on('drop', function (el, target) {
        var mealPositions = [];
        var orderId = getOrderId();

        $(target).children('li').each(function(index, li) {
            var mealId = $(li).data('id');
            mealPositions.push(mealId);
        });

        if (mealPositions.length > 0) {
            OrderMethods.setMealPositions.call({ orderId: orderId, meals: mealPositions }, function (err, res) {
                handleError(err);
            });
        } else {
            postal.publish({
                topic: 'toast',
                data: {
                    error: {
                        message: 'There was an error reordering the meals. Please refresh the page.'
                    }
                }
            });
        }
    });
}

function setupDraggableOfMealItems() {
    setupDraggable('.food-item', isAuthorizedToUpdateMeal(), function(draggableElement) {
        var $draggable = $(draggableElement);
        var newImg = $draggable.find('img')[0].cloneNode(true);

        newImg.style.width = '80px';
        newImg.style.height = '80px';
        newImg.style.position = 'absolute';
        newImg.style.top = draggableElement.offsetTop + 'px';
        newImg.style.left = (draggableElement.offsetLeft + 50) + 'px';
        newImg.style.zIndex = 10;
        newImg.dataset.id = $draggable.data('id');

        $(draggableElement).parents('.dropzone')[0].appendChild(newImg);

        return newImg;
    });
}

function setupDropTargetOfMealItems() {
    var isValidTarget = function(event) {
        var targetMealId = $(event.target).data('id');
        var sourceMealId = $(event.relatedTarget).parents('.dropzone').data('id');

        return !sourceMealId || (targetMealId !== sourceMealId);
    };

    interact('.orderDetailsPart .dropzone').dropzone({
        overlap: 0.01,
        ondropactivate: function (event) {
            if (isValidTarget(event)) {
                event.target.classList.add('drop-active');
            }
        },
        ondropdeactivate: function (event) {
            event.target.classList.remove('drop-active');
        },
        ondrop: function (event) {
            if (isValidTarget(event)) {
                var targetMealId = $(event.target).data('id');
                var $food = $(event.relatedTarget);
                var foodId = $food.data('id');
                var sourceMealId = $food.parents('.dropzone').data('id');

                if (foodId && sourceMealId && Meteor.userId()) {
                    moveFood(sourceMealId, foodId, targetMealId);
                } else {
                    addFood(targetMealId, $food);
                }
            }
        }
    });
}

function isAuthorizedToUpdateMeal() {
    return !Template.instance().data.orderId || Orders.findOne().userId == Meteor.userId();
}

function getOrderId() {
    var order = Orders.findOne();

    return order && order['_id'];
}

function editIngredient(mealId, ingredientId, measurement, measurementType) {
    var meal = getMeal(mealId);
    var ingredient = _.find(meal.items, i => i._id === ingredientId);

    if (ingredient) {
        delete ingredient.measurement;
        delete ingredient.measurementType;

        if (measurement) {
            ingredient.measurement = measurement;
            ingredient.measurementType = measurementType;
        }

        MealMethods.editMealFood.call({ mealId: mealId, food: ingredient }, methodReturnHandler);
    }
}

function removeFood(mealId, food) {
    if (Meteor.userId()) {
        MealMethods.removeFoodFromMeal.call({ mealId: mealId, _id: food['_id'] }, methodReturnHandler);
    } else {
        var meal = getMeal(mealId);
        var foodIndex = _.findIndex(meal.items, i => i['_id'] === food['_id']);

        if (foodIndex >= 0) {
            meal.items.splice(foodIndex, 1);
            LocalMeals.update(meal['_id'], meal);
        }
    }
}

function moveFood(sourceMealId, foodId, targetMealId) {
    if (Meteor.userId()) {
        var sourceMeal = getMeal(sourceMealId);
        var oldFood = _.find(sourceMeal.items || [], item => item._id === foodId);

        MealMethods.moveFood.call({ 'sourceMealId': sourceMealId, 'sourceFood': oldFood, 'targetMealId': targetMealId }, methodReturnHandler);
     }
}

function addFood(mealId, $food) {
    var meal = getMeal(mealId);
    var foodId = $food.data('id');
    var foodType = $food.data('type');
    var food = Recipes.findOne(foodId) || Ingredients.findOne(foodId);
    var mealFood = {
        _id: Random.id(),
        id: foodId,
        name: food.name,
        type: foodType,
        imageUrl: food.imageUrl
    };

    if (Meteor.userId()) {
        MealMethods.addFoodToMeal.call({ mealId: mealId, food: mealFood }, methodReturnHandler);
    } else {
        meal.items.push(mealFood);
        LocalMeals.update(meal['_id'], meal);
    }
}

function addPlaceholderFood(mealId, foodName) {
    var mealFood = {
        _id: Random.id(),
        id: 'PlaceholderId',
        name: foodName,
        type: 'placeholder',
        imageUrl: '/images/default-placeholder.png'
    };

    if (!Meteor.userId()) {
        //todo error
    } else {
        MealMethods.addFoodToMeal.call({ mealId: mealId, food: mealFood }, methodReturnHandler);
    }
}

function getMealsCursor() {
    var orderId = Template.instance().data.orderId;
    var publication = orderId ? 'orderAndMeals' : 'lastOrderAndMeals';

    if (orderId || Meteor.userId()) {
        var order = Orders.findOne();
        var mealsMap = order && order.meals && _.reduce(order.meals, function(map, val, index) {
            map[val] = index + 1;
            return map;
        }, {});

        return Meals.findFromPublication(publication, {}, {
            sort: { createdDate: 1 },
            transform: function(meal) {
                var mealOrder = mealsMap && mealsMap[meal._id];
                meal.order = mealOrder || 100;
                return meal;
            }
        });
    } else {
        return LocalMeals.find();
    }
}

function getMeal(mealId) {
    var coll = !!Meteor.userId() ? Meals : LocalMeals;

    return coll.findOne(mealId);
}

function methodReturnHandler(err, res) {
    if (err) {
        postal.publish({
            topic: 'toast',
            data: {
                error: err
            }
        });
    }
}

function handleError(err) {
    if (err) {
        postal.publish({
            topic: 'toast',
            data: {
                error: err
            }
        });
        return true;
    }
}
