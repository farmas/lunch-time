import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session'
import * as OrderMethods from '../../api/orders/orders.methods.js';
import postal from 'postal';

import './shoppingListPart.html';

Template.shoppingListPart.onCreated(function() {
    this.shoppingList = new ReactiveVar();
});

Template.shoppingListPart.onRendered(function() {
    var template = this;
    var meals = Session.get('meals');
    var methodReturnHandler = function(err, res) {
        if (err) {
            postal.publish({
                topic: 'toast',
                data: {
                    error: err
                }
            });
        } else {
            console.log('Retrieved shopping list', res);
            template.shoppingList.set(res);
        }
    };

    if (this.data.orderId) {
        OrderMethods.getShoppingList.call({ orderId: this.data.orderId }, methodReturnHandler);
    } else if (meals) {
        OrderMethods.getShoppingListFromMeals.call({ meals: meals }, methodReturnHandler);
    }
});

Template.shoppingListPart.helpers({
    shoppingList() {
        return Template.instance().shoppingList.get();
    },
    hasMeasuredAmount(ingredient) {
        return ingredient.amounts && _.some(ingredient.amounts, i => !!i.quantity);
    }
});