import { Meteor } from 'meteor/meteor';
import { Recipes } from '../../api/recipes/recipes.js';
import { RecipeSchema } from '../../api/recipes/recipes.js';
import { MeasurementTypes } from '../../api/recipes/recipes.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Ingredients } from '../../api/ingredients/ingredients.js';
import * as RecipeMethods from '../../api/recipes/recipes.methods.js';
import './recipeEditPart.html';
import postal from 'postal';
import ko from 'knockout';
import dragula from 'dragula';
import interact from 'interact.js';

function StepViewModel(step) {
    this.text = ko.observable(step);
}

function IngredientViewModel(ingredientId, ingredient) {
    this.ingredientId = ingredientId;
    this.name = ko.observable(ingredient.name);
    this.measurementType = ko.observable(ingredient.measurementType);
    this.measurement = ko.observable(ingredient.measurement);
}

function RecipeViewModel() {
    var self = this;
    this.name = ko.observable();
    this.sourceUrl = ko.observable();
    this.imageUrl = ko.observable();
    this.author = ko.observable();
    this.directions = ko.observableArray([]);
    this.ingredients = ko.observableArray([]);
    this.measurementTypes = MeasurementTypes;

    this.update = function() {
        var recipe = ko.toJS(self);
        recipe.directions = _.map(recipe.directions, s => s.text);

        RecipeSchema.clean(recipe);
        console.log('Update Recipe', recipe);

        var validationContext = RecipeSchema.namedContext();
        if(!validationContext.validate(recipe)) {
            var errorObj = validationContext.getErrorObject();
            postal.publish({ topic: 'toast', data: { error: errorObj, message: errorObj.message } });
        } else {
            RecipeMethods.addOrUpdate.call({ recipe: recipe}, function(err) {
                if (err) {
                    postal.publish({ topic: 'toast', data: { error: err }});
                } else {
                    postal.publish({ topic: 'toast', data: { message: 'Recipe has been saved.' }})
                }
            });
        }
    }

    this.addStep = function() {
        var $input = $('.step-add');

        self.directions.push(new StepViewModel($input.val()));

        $input.val('');
    }

    this.removeStep = function(step) {
        self.directions.remove(step);
    }

    this.addIngredient = function(ingredientId) {
        var ingredient = Ingredients.findOne(ingredientId);

        self.ingredients.push(new IngredientViewModel(ingredientId, ingredient));
    }

    this.removeIngredient = function(ingredient) {
        self.ingredients.remove(ingredient);
    }
}

Template.recipeEditPart.onCreated(function() {
    var recipeId = this.data.recipeId;
    var viewModel = this.viewModel = new RecipeViewModel();
    viewModel._id = recipeId;

    Meteor.subscribe('recipe', recipeId, function() {
        var recipe = Recipes.findOne(recipeId);

        if (recipe) {
            viewModel.name(recipe.name);
            viewModel.sourceUrl(recipe.sourceUrl);
            viewModel.imageUrl(recipe.imageUrl);
            viewModel.author(recipe.author);
            viewModel.directions(_.map(recipe.directions, s => new StepViewModel(s)));
            viewModel.ingredients(_.map(recipe.ingredients, i => new IngredientViewModel(i.ingredientId, i)));
        }
    });
});

Template.recipeEditPart.onRendered(function() {
    ko.applyBindings(this.viewModel, this.find('.recipeEditPart'));

    setupIngredientsDropZone(this.viewModel);
    setupSortableList($('.ingredients-list')[0], this.viewModel.ingredients);
    setupSortableList($('.directions-list')[0], this.viewModel.directions);
});

Template.recipeEditPart.onDestroyed(function() {
    var node = this.find('.recipeEditPart');

    if (node) {
        ko.cleanNode(node);
    }
});

Template.recipeEditPart.helpers({
    exists() {
        var recipeId = Template.instance().data.recipeId;
        return Recipes.find({ _id: recipeId }).count() > 0;
    }
});

Template.recipeEditPart.events({
    'click .recipe-cancel': goBack,
    'click .recipe-delete': function(event, template) {
        if (confirm('Are you sure you want to delete this recipe?')) {
            var recipeId = template.viewModel._id;
            RecipeMethods.removeRecipe.call({ id: recipeId }, function(err) {
                if (err) {
                    postal.publish({ topic: 'toast', data: { error: err }});
                } else {
                    goBack();
                }
            });
        }
    }
});

function goBack() {
    var route = FlowRouter.getQueryParam('origin') || 'orders';
    FlowRouter.go(route);
}

function setupIngredientsDropZone(viewModel) {
    interact('.ingredients-content').dropzone({
        overlap: 0.01,
        ondropactivate: function (event) {
            event.target.classList.add('drop-active');
        },
        ondropdeactivate: function (event) {
            event.target.classList.remove('drop-active');
        },
        ondragenter: function (event) {
            event.target.classList.add('drop-target');
        },
        ondragleave: function (event) {
            event.target.classList.remove('drop-target');
        },
        ondrop: function (event) {
            event.target.classList.remove('drop-target');
            event.target.classList.remove('drop-active');
            var ingredientId = $(event.relatedTarget).data('id');
            viewModel.addIngredient(ingredientId);
        }
    });
}

function setupSortableList(htmlList, observableArray) {
    var drake = dragula([htmlList], {
        moves: function(el, container, handle) {
            return !!$(handle).parents('.list-item-handle').length;
        }
    }).on('drop', function (el) {
        var item = ko.dataFor(el);

        observableArray.remove(item);
        observableArray.splice($(el).index(), 0, item);

        drake.remove();
    });
}
