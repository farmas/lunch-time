import { FlowRouter } from 'meteor/kadira:flow-router';
import postal from 'postal';
import fractional from 'fractional';
import { formatMeasurement } from '../../api/orders/orders.methods.js';

import './foodItemPart.html';

Template.foodItemPart.helpers({
    foodAmount(food) {
        if (food.type === 'ingredient') {
            var measurement = formatMeasurement(food);

            if (measurement) {
                return `(${measurement})`;
            }
        }
    },
    recipeEditUrl() {
        var food = Template.instance().data.food;

        if (food && food.type === 'recipe') {
            var path = FlowRouter.path('recipes.edit', { recipeId: food.id });
            return window.location.origin + path;
        }
    },
    isIngredient() {
        var food = Template.instance().data.food;

        return food && food.type === 'ingredient';
    }
});

Template.foodItemPart.events({
    'click .food-action-edit': function(event, template) {
        if (template.data.food.type === 'ingredient') {
            event.preventDefault();
            postal.publish({
                topic: 'ingredient.edit',
                data: {
                    mealId: template.data.mealId,
                    ingredient: template.data.food
                }
            });
        }
    },
    'click .food-action-view': function(event, template) {
        event.preventDefault();

        if (template.data.food.type === 'recipe') {
            postal.publish({
                topic: 'recipe.view',
                data: {
                    id: template.data.food.id
                }
            });
        }
    },
    'click .food-action-delete': function(event, template) {
        event.preventDefault();

        postal.publish({
            topic: 'food.removed',
            data: {
                mealId: template.data.mealId,
                food: template.data.food
            }
        });
    }
});