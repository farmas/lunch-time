import { ReactiveVar } from 'meteor/reactive-var';
import { Recipes } from '../../api/recipes/recipes.js';
import { formatMeasurement } from '../../api/orders/orders.methods.js';
import postal from 'postal';
import fractional from 'fractional';

import './recipeDetailsPart.html';

Template.recipeDetailsPart.onCreated(function() {
    var template = this;

    template.recipeId = new ReactiveVar(this.data && this.data.recipeId);
    template.postalSub = postal.subscribe({
        topic: 'recipe.selected',
        callback: (data) => {
            template.recipeId.set(data.id);
        }
    });

    template.autorun(function() {
        template.subscribe('recipe', template.recipeId.get());
    });
});

Template.recipeDetailsPart.onDestroyed(function() {
    postal.unsubscribe(this.postalSub);
});

Template.recipeDetailsPart.helpers({
    recipe() {
        return Recipes.findOne({ '_id': Template.instance().recipeId.get() })
    },
    formatMeasurement(ingredient) {
        return formatMeasurement(ingredient);
    }
});

Template.registerHelper('increment', function (index) {
    return index + 1;
});