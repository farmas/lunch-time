import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '../../ui/mainLayout.js';
import '../../ui/pages/orderPage.js';
import '../../ui/pages/ingredientsPage.js';
import '../../ui/pages/recipesPage.js';
import '../../ui/pages/recipeEditPage.js';
import '../../ui/pages/shoppingListPage.js';
import '../../ui/pages/aboutPage.js';
import '../../ui/pages/orderViewPage.js';
import '../../ui/pages/orderViewMobilePage.js';
import '../../ui/pages/recipeViewPage.js';

var navigatableRoutes = FlowRouter.group({ name: 'navigatable'});
var sections = {
    orders: 'orders',
    recipes: 'recipes',
    ingredients: 'ingredients',
};

navigatableRoutes.route('/', {
    name: 'orders',
    displayName: 'Orders',
    action() {
        BlazeLayout.render('mainLayout', { content: 'orderPage'});
    }
});

navigatableRoutes.route('/recipes', {
    name: 'recipes',
    displayName: 'Recipes',
    action() {
        BlazeLayout.render('mainLayout', { content: 'recipesPage' });
    }
});

navigatableRoutes.route('/ingredients', {
    name: 'ingredients',
    displayName: 'Ingredients',
    action() {
        BlazeLayout.render('mainLayout', { content: 'ingredientsPage' });
    }
});

navigatableRoutes.route('/about', {
    name: 'about',
    displayName: 'About',
    action() {
        BlazeLayout.render('mainLayout', { content: 'aboutPage' });
    }
});

FlowRouter.route('/recipes/:recipeId/edit', {
    name: 'recipes.edit',
    section: sections.recipes,
    action() {
        BlazeLayout.render('mainLayout', { content: 'recipeEditPage' });
    }
});

FlowRouter.route('/recipes/:recipeId/view', {
    name: 'recipes.view',
    section: sections.recipes,
    action() {
        BlazeLayout.render('mainLayout', { content: 'recipeViewPage' });
    }
});

FlowRouter.route('/orders/:orderId/shoppingList', {
    name: 'orders.shoppingList',
    section: sections.orders,
    action() {
        BlazeLayout.render('mainLayout', { content: 'shoppingListPage' });
    }
});

FlowRouter.route('/orders/:orderId', {
    name: 'orders.view',
    section: sections.orders,
    action() {
        BlazeLayout.render('mainLayout', { content: 'orderViewPage' });
    }
});

FlowRouter.route('/orders/:orderId/mobile', {
    name: 'orders.mobile.view',
    section: sections.orders,
    action() {
        BlazeLayout.render('mainLayout', { content: 'orderViewMobilePage' });
    }
});