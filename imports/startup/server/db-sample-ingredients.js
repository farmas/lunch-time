export const sampleIngredients = [
    {
        name: "Gemelli pasta",
        imageUrl: "http://cdn.foodfacts.com/images/products/22364.jpg",
        estimatedPrice: 0
    },
    {
        name: "Unsalted butter",
        imageUrl: "http://cdn.foodfacts.com/images/products/68566.jpg",
        estimatedPrice: 0
    },
    {
        name: "Leek",
        imageUrl: "http://cdn.foodfacts.com/images/products/209172.jpg",
        estimatedPrice: 0
    },
    {
        name: "Kosher salt",
        imageUrl: "http://cdn.foodfacts.com/images/products/57964.jpg",
        estimatedPrice: 0
    },
    {
        name: "Black pepper",
        imageUrl: "http://cdn.foodfacts.com/images/products/198396.jpg",
        estimatedPrice: 0
    },
    {
        name: "Shrimp",
        imageUrl: "http://cdn.foodfacts.com/images/products/24790.jpg",
        estimatedPrice: 0
    },
    {
        name: "Lemon",
        imageUrl: "http://cdn.foodfacts.com/images/products/209097.jpg",
        estimatedPrice: 0
    },
    {
        name: "Heavy Cream",
        imageUrl: "http://cdn.foodfacts.com/images/products/73424.jpg",
        estimatedPrice: 0
    },
    {
        name: "Spinach",
        imageUrl: "http://cdn.foodfacts.com/images/products/21565.jpg",
        estimatedPrice: 0
    },
    {
        name: "White Rice",
        imageUrl: "http://cdn.foodfacts.com/images/products/22331.jpg",
        estimatedPrice: 0
    },
    {
        name: "Honey",
        imageUrl: "http://cdn.foodfacts.com/images/products/139131.jpg",
        estimatedPrice: 0
    },
    {
        name: "Soy sauce",
        imageUrl: "http://cdn.foodfacts.com/images/products/22721.jpg",
        estimatedPrice: 0
    },
    {
        name: "Crushed Red Pepper",
        imageUrl: "http://cdn.foodfacts.com/images/products/12653.jpg",
        estimatedPrice: 0
    },
    {
        name: "Salmon fillets",
        imageUrl: "http://cdn.foodfacts.com/images/products/203682.jpg",
        estimatedPrice: 0
    },
    {
        name: "Bok choy",
        imageUrl: "http://cdn.foodfacts.com/images/products/209644.jpg",
        estimatedPrice: 0
    }
];