import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { Ingredients } from '../../api/ingredients/ingredients.js';
import { Recipes } from '../../api/recipes/recipes.js';
import { sampleIngredients } from './db-sample-ingredients.js';
import { sampleRecipes } from './db-sample-recipes.js';
import { _ } from 'meteor/underscore';

var findIngredientId = function(name) {
    name = name.toLowerCase();
    var ingredient = _.find(sampleIngredients, (i) => {
        return i.name.toLowerCase() === name;
    });

    var ingredientId = ingredient && ingredient["_id"];
    if (!ingredientId) {
        throw new Error('Ingredient id not found for ' + name);
    }

    return ingredientId;
}

Meteor.startup(() => {
    Ingredients._ensureIndex({
      'name': 1
    });

    Recipes._ensureIndex({
        'name': 1
    });

    if (Ingredients.find().count() == 0) {
        sampleIngredients.forEach((i) => {
            i['_id'] = Random.id();
            Ingredients.insert(i);
        });
    }

    if (Recipes.find().count() == 0) {
        sampleRecipes.forEach((r) => {
            r.ingredients.forEach((i) => {
                i.ingredientId = findIngredientId(i.normalizedName || i.name);
                delete i.normalizedName;
            });

            Recipes.insert(r);
        });
    }
});