export const sampleRecipes = [
  {
    "name": "Shrimp, Leek, and Spinach Pasta",
    "serves": "4",
    "sourceUrl": "http://www.realsimple.com/food-recipes/browse-all-recipes/shrimp-leek-spinach-pasta",
    "imageUrl": "http://cdn-image.realsimple.com/sites/default/files/styles/rs_main_image/public/image/images/1104/shrimp-leek-pasta_300.jpg?itok=L2BAwEXA",
    "author": "Abigail Chipley and Charlyne Mattox",
    "directions": [
      "Cook the pasta according to the package directions; drain and return it to the pot.",
      "Meanwhile, heat the butter in a large skillet over medium heat. Add the leeks, ½ teaspoon salt, and ¼ teaspoon pepper and cook, stirring occasionally, until the leeks have softened, 3 to 5 minutes.",
      "Add the shrimp and lemon zest and cook, tossing frequently, until the shrimp is opaque throughout, 4 to 5 minutes more.",
      "Add the cream and ½ teaspoon salt to the pasta in the pot and cook over medium heat, stirring, until slightly thickened, 1 to 2 minutes. Add the shrimp mixture and the spinach and toss to combine."
    ],
    "ingredients": [
    ]
  },
   {
    "name": "Spicy Salmon With Bok Choy and Rice",
    "serves": "4",
    "sourceUrl": "http://www.realsimple.com/food-recipes/browse-all-recipes/spicy-salmon-bok-choy-rice",
    "imageUrl": "http://cdn-image.realsimple.com/sites/default/files/styles/rs_main_image/public/image/images/1302/bokchoy-salmon-rice_300.jpg?itok=3ZdRfdzr",
    "author": "Charlyne Mattox",
    "directions": [
      "Cook the rice according to the package directions.",
      "Meanwhile, combine the honey, soy sauce, and red pepper. On a foil-lined rimmed baking sheet, broil the salmon fillets until opaque throughout, 8 to 10 minutes, basting with the honey mixture during the last 3 minutes.",
      "Steam baby bok choy (cut into quarters) until tender, 8 to 10 minutes. Serve with the salmon and rice."
    ],
    "ingredients": [
    ]
  }
];