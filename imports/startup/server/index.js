import './db-setup.js';
import './serviceConfig.js';

// methods
import '../../api/orders/orders.methods.js';
import '../../api/meals/meals.methods.js';
import '../../api/ingredients/ingredients.methods.js';
import '../../api/recipes/recipes.methods.js';

// publications
import '../../api/ingredients/ingredients.publications.js';
import '../../api/recipes/recipes.publications.js';
import '../../api/orders/orders.publications.js';