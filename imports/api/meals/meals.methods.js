import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Meals } from './meals.js';
import { Orders } from '../orders/orders.js';
import { Random } from 'meteor/random';
import * as Mixins from '../method.mixins.js';

export const renameMeal = new ValidatedMethod({
    name: 'meal.rename',
    validate: new SimpleSchema({
        mealId: { type: String },
        name: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated, Mixins.mealOwner],
    run({ mealId, name }) {
        Meals.update(mealId, { $set: { name: name }});
    }
});

export const addMeal = new ValidatedMethod({
    name: 'meals.add',
    validate: new SimpleSchema({
        name: { type: String },
        orderId: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated, Mixins.orderOwner],
    run({ name, orderId }) {
        var mealId = Random.id();
        Meals.insert({
            _id: mealId,
            orderId: orderId,
            userId: this.userId,
            name: name,
            createdDate: new Date(),
            items: []
        });

        Orders.update(orderId, { $push: { meals: mealId }});
    }
});

export const removeMeal = new ValidatedMethod({
    name: 'meals.remove',
    validate: new SimpleSchema({
        mealId: { type: String },
        orderId: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated],
    run({ mealId, orderId }) {
        Meals.remove({ _id: mealId, userId: this.userId });

        Orders.update( orderId, { $pull: { meals: mealId }});
    }
});

var foodSchema = new SimpleSchema({
    _id: { type: String },
    id: { type: String },
    name: { type: String },
    type: { type: String },
    imageUrl: { type: String },
    measurement: { type: Number, decimal: true, optional: true },
    measurementType: { type: String, optional: true}
});

export const moveFood = new ValidatedMethod({
    name: 'meals.moveFood',
    validate: new SimpleSchema({
        sourceMealId: { type: String },
        sourceFood: { type: foodSchema },
        targetMealId: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated],
    run({ sourceMealId, sourceFood, targetMealId }) {
        if (Meals.find({ _id: sourceMealId, userId: this.userId}).count() === 0) {
            throw new Meteor.Error('notfound', 'The source meal specified was not found for this user.');
        }

        if (Meals.find({ _id: targetMealId, userId: this.userId}).count() === 0) {
            throw new Meteor.Error('notfound', 'The target meal specified was not found for this user.');
        }

        var targetFood = _.extend({}, sourceFood, { _id: Random.id() });

        Meals.update(targetMealId, { $push: { items: targetFood }});
        Meals.update(sourceMealId, { $pull: { items: { _id: sourceFood._id }}});
    }
});

export const addFoodToMeal = new ValidatedMethod({
    name: 'meals.addFood',
    validate: new SimpleSchema({
        mealId: { type: String },
        food: { type: foodSchema }
    }).validator(),
    mixins: [Mixins.authenticated, Mixins.mealOwner],
    run({ mealId, food }) {
        Meals.update(mealId, { $push: { items: food }});
    }
});

export const removeFoodFromMeal = new ValidatedMethod({
    name: 'meals.removeFood',
    validate: new SimpleSchema({
        mealId: { type: String },
        _id: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated, Mixins.mealOwner],
    run({ mealId, _id }) {
        Meals.update(mealId, { $pull: { items: { _id: _id }}});
    }
});

export const editMealFood = new ValidatedMethod({
    name: 'meals.editFood',
    validate: new SimpleSchema({
        mealId: { type: String },
        food: { type: foodSchema }
    }).validator({ clean: true }),
    mixins: [Mixins.authenticated, Mixins.mealOwner],
    run({ mealId, food }) {
        Meals.update(
            { '_id': mealId, 'userId': this.userId, 'items._id': food._id },
            { $set: { 'items.$': food }});
    }
});