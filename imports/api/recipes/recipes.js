import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Recipes = new Mongo.Collection('recipes');

IngredientSchema = new SimpleSchema({
    ingredientId: { type: String },
    name: { type: String },
    measurementType: { type: String, optional: true },
    measurement: { type: Number, decimal: true, optional: true }
});

export const RecipeSchema = new SimpleSchema({
    _id: { type: String },
    name: { type: String },
    serves: { type: Number, optional: true },
    sourceUrl: { type: String, optional: true },
    imageUrl: { type: String, optional: true, defaultValue: '/images/default-recipe.png' },
    author: { type: String, optional: true },
    directions: { type: [String] },
    ingredients: { type: [IngredientSchema] }
});

export const MeasurementTypes = ['pound', 'tablespoon', 'cup', 'ounce', 'teaspoon', 'gram', 'kilo'].sort();