import { Meteor } from 'meteor/meteor';
import { Recipes } from './recipes.js';
import { FindFromPublication} from 'meteor/percolate:find-from-publication';

if (Meteor.isServer) {
    Meteor.publish('recipe', (id) => {
        return Recipes.find({ '_id': id });
    });

    FindFromPublication.publish('recipes', (limit, nameQuery) => {
        var query = {};
        var options = {
            fields: { name: 1, '_id': 1, userId: 1, userName: 1, imageUrl: 1 },
            sort: { name: 1 },
            limit: limit
        };

        if (nameQuery) {
            query = {
                name: {
                    $regex: nameQuery,
                    $options: 'i'
                }
            }
        }

        return Recipes.find(query, options);
    });
}