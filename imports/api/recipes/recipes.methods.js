import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Recipes } from './recipes.js';
import { RecipeSchema } from './recipes.js';
import * as Mixins from '../method.mixins.js';

export const addOrUpdate = new ValidatedMethod({
    name: 'recipe.addOrUpdate',
    validate: new SimpleSchema({
        recipe: { type: RecipeSchema }
    }).validator({ clean: true }),
    mixins: [Mixins.authenticated],
    run({ recipe }) {
        var user = Meteor.isServer && Meteor.user();
        var email = user && user.services.google.email;
        var displayName = user && user.profile.name;

        recipe.userId = this.userId;
        recipe.userName = email && email.substring(0, email.indexOf('@'));

        if (!recipe.author && displayName) {
            recipe.author = displayName;
        }

        Recipes.update({ _id: recipe._id }, recipe, { upsert: true });
    }
});

export const removeRecipe = new ValidatedMethod({
    name: 'recipe.remove',
    validate: new SimpleSchema({
        id: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated],
    run({ id }) {
        Recipes.remove({ _id: id, userId: this.userId });
    }
});