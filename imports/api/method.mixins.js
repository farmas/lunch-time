import { Meals } from './meals/meals.js';
import { Orders } from './orders/orders.js';

export const authenticated = createMixin(function() {
    if (!this.userId) {
        throw new Meteor.Error('unauthorized', 'You must be logged in to access this method.');
    };
});

export const mealOwner = createMixin(function({ mealId }) {
    if (Meals.find({ _id: mealId, userId: this.userId}).count() === 0) {
        throw new Meteor.Error('notfound', 'The meal specified was not found for this user.');
    }
});

export const orderOwner = createMixin(function({ orderId }) {
    if (Orders.find({ _id: orderId, userId: this.userId}).count() === 0) {
        throw new Meteor.Error('notfound', 'The order specified was not found for this user.');
    }
});

function createMixin(callback) {
    var myMixin = function(methodOptions) {
        const runFunc = methodOptions.run;
        methodOptions.run = function() {
            callback.call(this, ...arguments);
            return runFunc.call(this, ...arguments);
        }

        return methodOptions;
    }

    return myMixin;
}