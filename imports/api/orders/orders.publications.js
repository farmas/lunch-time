import { Meteor } from 'meteor/meteor';
import { Orders } from './orders.js';
import { Meals } from '../meals/meals.js';
import { FindFromPublication} from 'meteor/percolate:find-from-publication';

if (Meteor.isServer) {
    FindFromPublication.publish('lastOrderAndMeals', function() {
        var orderCursor = Orders.find({ userId: this.userId }, { sort: { createdDate: -1}, limit: 1 });
        var cursors = [orderCursor];

        var orders = orderCursor.fetch();
        var order = orders && orders.length && orders[0];

        if (order) {
            cursors.push(Meals.find({ orderId: order['_id'] }, { sort: { createdDate: 1 }}));
        }

        return cursors;
    });

    FindFromPublication.publish('orderAndMeals', function(orderId) {
        return [
            Orders.find({ _id: orderId }),
            Meals.find({ orderId: orderId }, { sort: { createdDate: 1 }})
        ];
    });
}