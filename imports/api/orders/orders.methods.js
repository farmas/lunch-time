import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Orders } from './orders.js';
import { Ingredients } from '../ingredients/ingredients.js';
import { Recipes } from '../recipes/recipes.js';
import { Meals } from '../meals/meals.js';
import { Random } from 'meteor/random';
import { Email } from 'meteor/email'
import fractional from 'fractional';
import * as Mixins from '../method.mixins.js';

export const formatMeasurement = function(amount) {
    if (amount.measurement) {
        var quantity = new fractional.Fraction(amount.measurement).toString();
        if (amount.measurementType && amount.measurementType !== 'unit') {
            quantity += ` ${amount.measurementType}s`;
        }
        return quantity;
    }
}

export const ensureOrderExists = new ValidatedMethod({
    name: 'orders.ensureExists',
    validate: null,
    run() {
        if (Meteor.isServer && this.userId) {
            var order = Orders.findOne({ userId: this.userId });

            if (!order) {
                order = {
                    _id: Random.id(),
                    userId: this.userId,
                    createdDate: new Date(),
                    meals: []
                };

                Orders.insert(order);
            }

            if (Meals.find({ orderId: order['_id'] }).count() === 0) {
                var orderId = order['_id'];
                var mealId = Random.id();
                Meals.insert({
                    _id: mealId,
                    orderId: orderId,
                    userId: this.userId,
                    name: 'Sample Meal (Server)',
                    items: [],
                    createdDate: new Date()
                });

                Orders.update(orderId, { $set: { meals: [mealId] }});
            }
        }
    }
});

export const setMealPositions = new ValidatedMethod({
    name: 'order.setMealPositions',
    validate: new SimpleSchema({
        orderId: { type: String },
        meals: { type: [String] }
    }).validator({ clean: true }),
    mixins: [Mixins.authenticated, Mixins.orderOwner],
    run({ orderId, meals }) {
        Orders.update(orderId, { $set: { meals: meals }});
    }
})

export const mailShoppingList = new ValidatedMethod({
    name: 'order.mailShoppingList',
    validate: new SimpleSchema({
        orderId: { type: String },
        email: { type: String }
    }).validator({ clean: true }),
    mixins: [Mixins.authenticated, Mixins.orderOwner],
    run({ orderId, email }) {
        if (Meteor.isClient) {
            return;
        }

        console.log('Sending mail to ', email);

        var meals = Meals.find({ orderId : orderId }).fetch();
        var shoppingList = buildShoppingListFromMeals(meals);
        var html = Handlebars.templates['shoppingList'](shoppingList);

        Email.send({
          to: email,
          from: "fsilva.armas@gmail.com",
          subject: "Lunch Time: Your Shopping List",
          html: html,
        });
    }
});

export const getShoppingListFromMeals = new ValidatedMethod({
    name: 'order.getAnonymousShoppingList',
    validate: null,
    run({ meals }) {
        if (Meteor.isClient) {
            return;
        }

        return buildShoppingListFromMeals(meals);
    }
});

export const getShoppingList = new ValidatedMethod({
    name: 'order.getShoppingList',
    validate: new SimpleSchema({
        orderId: { type: String }
    }).validator({ clean: true}),
    run({ orderId }) {
        if (Meteor.isClient) {
            return;
        }

        console.log('Getting shopping list for order', orderId);
        var meals = Meals.find({ orderId : orderId }).fetch();

        return buildShoppingListFromMeals(meals);
    }
});

function buildShoppingListFromMeals(meals) {
    var data = { map: {}, recipes: {}, lostIngredients: [], lostRecipes: [] };

    _.each(meals, meal => processMeal(data, meal));

    processIngredientsFromRecipes(data);

    var categories = buildCategories(data);

    return {
        categories: categories,
        lostIngredients: data.lostIngredients,
        lostRecipes: data.lostRecipes
    };
}

function processMeal(data, meal) {
    _.each(meal.items, function(item) {
        if (item.type === 'ingredient') {
            processIngredient(data, item, meal.name);
        } else if (item.type === 'recipe') {
            processRecipe(data, item, meal);
        } else {
            // no op, place holder items are not included in the shopping list.
        }
    });
}

function processRecipe(data, recipe, meal) {
    var id = recipe.id;

    if(!data.recipes[id]) {
        data.recipes[id] = {
            name: recipe.name,
            meals: [meal.name]
        };
    } else {
        data.recipes[id].meals.push(meal.name);
    }
}

function processIngredient(data, ingredient, meal, recipe) {
    var id = ingredient.id || ingredient.ingredientId;
    var newRef = {
        meal: meal,
        recipe: recipe,
        measurement: ingredient.measurement,
        measurementType: ingredient.measurementType
    };

    if (!data.map[id]) {
        data.map[id] = {
            name: ingredient.name,
            amountText: '',
            amounts: [],
            refs: []
        };
    }

    var item = data.map[id];
    item.refs.push(newRef);

    // calulate the running total of amounts.
    if (ingredient.measurement) {
        var measurementType = ingredient.measurementType || 'unit';
        var amount = _.find(item.amounts, i => i.measurementType === measurementType);

        if (!amount) {
            item.amounts.push({ measurement: ingredient.measurement, measurementType: measurementType });
        } else {
            amount.measurement += ingredient.measurement;
        }

        item.amountText = calculateAmountText(item.amounts);
    }
}

function calculateAmountText(amounts) {
    var formattedAmounts = _.chain(amounts)
        .map(formatMeasurement)
        .compact()
        .value();

    return formattedAmounts.join(" + ");
}

function processIngredientsFromRecipes(data) {
    var recipeIds = Object.keys(data.recipes);
    if (recipeIds.length === 0) {
        return;
    }

    var recipes = fetchRecipes(recipeIds);

    _.each(recipeIds, function(recipeId) {
        var recipeInfo = data.recipes[recipeId];
        var recipe = recipes[recipeId];
        var ingredients = recipe && recipe.ingredients;

        if (!recipe) {
            data.lostRecipes.push(recipeInfo);
        } else {
            _.each(recipeInfo.meals, function(meal) {
                _.each(ingredients, function(ingredient) {
                    processIngredient(data, ingredient, meal, recipeInfo.name);
                });
            });
        }
    });
}

function fetchRecipes(recipeIds) {
    var recipes = Recipes.find({ _id: { $in: recipeIds }}, { fields: { _id: 1, ingredients: 1 }}).fetch();

    return _.reduce(recipes, function(map, item) {
        map[item._id] = item;
        return map;
    }, {});
}

function buildCategories(data) {
    var ids = Object.keys(data.map);
    var ingredients = Ingredients.find({ _id: { $in: ids }}).fetch();
    var categories = {};

    _.each(ids, function(id) {
        var item = ingredients.find(i => i._id === id);
        var itemRef = data.map[id];

        if (!item) {
            data.lostIngredients.push(itemRef);
        } else {
            var cat = item.category || 'Other';

            if (!categories[cat]) {
                categories[cat] = {
                    name: cat,
                    items: []
                }
            }

            var items = categories[cat].items;
            if (!_.some(items, i => i.id === item._id)) {
                items.push(itemRef);
            }
        }
    });

    return _.map(Object.keys(categories).sort(), i => categories[i]);
}