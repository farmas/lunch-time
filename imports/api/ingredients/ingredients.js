import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Ingredients = new Mongo.Collection('ingredients');

export const IngredientSchema = new SimpleSchema({
    _id: { type: String },
    name: { type: String },
    category: { type: String, optional: true },
    imageUrl: { type: String, optional: true, defaultValue: '/images/default-ingredient.png' }
});

export const Categories = ['Produce', 'Meats', 'Dairy', 'Condiments', 'Pantry', 'Frozen', 'Snacks'].sort();