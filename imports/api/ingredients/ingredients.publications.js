import { Meteor } from 'meteor/meteor';
import { Ingredients } from './ingredients.js';
import { FindFromPublication} from 'meteor/percolate:find-from-publication';

if (Meteor.isServer) {
    FindFromPublication.publish('ingredients', function(limit, onlyMine, nameQuery) {
        var query = {};
        var options = {
            sort: { name: 1 },
            limit: limit
        };

         if (nameQuery) {
            query['name'] = {
                $regex: nameQuery,
                $options: 'i'
            };
        }

        if (onlyMine && this.userId) {
            query['userId'] = this.userId;
        }

        return Ingredients.find(query, options);
    });
}