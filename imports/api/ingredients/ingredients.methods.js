import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Ingredients } from '../ingredients/ingredients.js';
import { IngredientSchema } from '../ingredients/ingredients.js';
import * as Mixins from '../method.mixins.js';

export const addOrUpdate = new ValidatedMethod({
    name: 'ingredients.addOrUpdate',
    validate: new SimpleSchema({
        ingredient: { type: IngredientSchema }
    }).validator({ clean: true }),
    mixins: [Mixins.authenticated],
    run({ ingredient }) {
        var email = Meteor.isServer && Meteor.user().services.google.email;

        ingredient.userId = this.userId;
        ingredient.userName = email && email.substring(0, email.indexOf('@'));

        Ingredients.update({ _id: ingredient._id}, ingredient, { upsert: true});
    }
});

export const removeIngredient = new ValidatedMethod({
    name: 'ingredients.remove',
    validate: new SimpleSchema({
        id: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated],
    run({ id }) {
        Ingredients.remove({ _id: id, userId: this.userId });
    }
});