# README #

LunchTime is a sample application built with [meteor.js](https://www.meteor.com/).

### Setup/Run ###

* Install meteor.
* Clone repo.
* Run 'meteor npm install' from the repo directory.
* Run 'meteor' from the repo directory.

### Setup Google Auth

* Activate the Google Auth API
* Create a dev-settings.json file with:
```
{
    "clientId": "GOOGLE_CLIENT_ID",
    "secret": "GOOGLE_SECRET" 
}
```
* Run 'meteor --settings dev-settings.json' from root.

### Deploy to Heroku

* Follow the instruction to [deploy to Heroku with Git](https://devcenter.heroku.com/articles/git#prerequisites-install-git-and-the-heroku-cli).

### Watch it live ###

The latest code is running at https://mylunchtime.herokuapp.com/

### Follow the blog ###

Read my [blog series](http://www.federicosilva.net/search/label/meteor.js) written while developing the app.